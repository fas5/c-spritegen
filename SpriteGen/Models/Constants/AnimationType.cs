﻿namespace SpriteGen.Models.Constants
{
    public static class AnimationType
    {
        public const string Walk = "walk";

        public const string Slash = "slash";

        public const string Shoot = "shoot";

        public const string Spellcast = "spellcast";

        public const string Thrust = "thrust";

        public const string Hurt = "hurt";

    }
}
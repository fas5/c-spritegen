﻿namespace SpriteGen.Models.Constants
{
    public static class Race
    {
        public const string Dark = "dark";
        public const string Dark2 = "dark2";
        public const string DarkElf = "darkelf";
        public const string DarkElf2 = "darkelf2";
        public const string Light = "light";
        public const string Orc = "orc";
        public const string RedOrc = "redorc";
        public const string Skeleton = "skeleton";
        public const string Tanned = "tanned";
        public const string Tanned2 = "tanned2";
    }
}
﻿namespace SpriteGen.Models.Constants
{
    public static class Genre
    {
        public const string Female = "female";

        public const string Male = "male";
    }
}
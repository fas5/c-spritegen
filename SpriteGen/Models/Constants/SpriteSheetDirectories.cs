﻿namespace SpriteGen.Models.Constants
{
    public class SpriteSheetDirectories
    {
        public static string SpriteSheetFileType => "*.png";
        public static string Root => "/Content/Sprites/mvp";
        public static string Body => string.Concat(Root, "/body");
        public static string Nose => "/nose";
        public static string Ears => "/ears";
        public static string Eyes => string.Concat(Body, "/eyes");
        public static string Hair => string.Concat(Root, "/hair");
        public static string Torso => string.Concat(Root, "/torso");
        public static string Legs => string.Concat(Root, "/legs");
        public static string Feet => string.Concat(Root, "/feet");
        public static string Arms => string.Concat(Root, "/arms");
        public static string Hands => string.Concat(Root, "/hands");
        public static string Weapon => string.Concat(Root, "/weapon");
    }
}
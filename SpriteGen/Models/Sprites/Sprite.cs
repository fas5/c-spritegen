﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace SpriteGen.Models.Sprites
{
    public class Sprite
    {
        public string Id { get; set; }

        public string OriginalBody { get; set; }
        public string OriginalNose { get; set; }
        public string OriginalEars { get; set; }
        public string OriginalEyes { get; set; }
        public string OriginalHair { get; set; }
        public string OriginalTorso { get; set; }
        public string OriginalLegs { get; set; }
        public string OriginalFeet { get; set; }
        public string OriginalArms { get; set; }
        public string OriginalHands { get; set; }
        public string OriginalWeapon { get; set; }

        public string CutBody { get; set; }
        public string CutNose { get; set; }
        public string CutEars { get; set; }
        public string CutEyes { get; set; }
        public string CutHair { get; set; }
        public string CutTorso { get; set; }
        public string CutLegs { get; set; }
        public string CutFeet { get; set; }
        public string CutArms { get; set; }
        public string CutHands { get; set; }
        public string CutWeapon { get; set; }

        public List<string> OriginalSpriteSheets { get; set; }
        public Image MergedSpriteSheet { get; set; }
        public string MergedSpriteSheetBase64 { get; set; }

        public Sprite(string originalBody, string originalNose, string originalEars, string originalEyes, string originalHair, string originalTorso,
            string originalLegs, string originalFeet, string originalArms, string originalHands, string originalWeapon)
        {
            Id = Guid.NewGuid().ToString("N");

            OriginalBody = originalBody;
            OriginalNose = originalNose;
            OriginalEars = originalEars;
            OriginalEyes = originalEyes;
            OriginalHair = originalHair;
            OriginalTorso = originalTorso;
            OriginalLegs = originalLegs;
            OriginalFeet = originalFeet;
            OriginalArms = originalArms;
            OriginalHands = originalHands;
            OriginalWeapon = originalWeapon;

            OriginalSpriteSheets = new List<string>();
            OriginalSpriteSheets.AddRange(new[]
            {
                OriginalBody,
                OriginalNose,
                OriginalEars,
                OriginalEyes,
                OriginalHair,
                OriginalLegs,
                OriginalFeet,
                OriginalTorso,
                OriginalArms,
                OriginalHands,
                OriginalWeapon
            });

            // Setting merged image
            //MergedSpriteSheet = Util.MergeSpriteSheets(this);
            // Setting merged image as string (base64)
            //var byteArray = Util.ImageToByteArray(MergedSpriteSheet);
            //MergedSpriteSheetBase64 = Util.ByteArrayToBase64(byteArray);
        }
    }
}
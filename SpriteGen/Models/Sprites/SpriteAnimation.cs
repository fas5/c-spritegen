﻿using System.Collections.Generic;

namespace SpriteGen.Models.Sprites
{
    public class SpriteAnimation
    {
        public string Type { get; set; }

        public List<string> BackFrameList { get; set; }
        public List<string> LeftFrameList { get; set; }
        public List<string> FrontFrameList { get; set; }
        public List<string> RightFrameList { get; set; }

        public SpriteAnimation()
        {
            BackFrameList = new List<string>();
            LeftFrameList = new List<string>();
            FrontFrameList = new List<string>();
            RightFrameList = new List<string>();
        }
    }
}
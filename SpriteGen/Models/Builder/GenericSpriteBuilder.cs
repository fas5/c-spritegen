﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MersenneTwister;
using SpriteGen.Models.Algorithms;
using SpriteGen.Models.Constants;
using SpriteGen.Models.Sprites;

namespace SpriteGen.Models.Builder
{
    public class GenericSpriteBuilder
    {
        private readonly Configuration _config = Configuration.GetInstance();

        private readonly Random _mersenneTwister;

        private readonly string _race;

        public List<string> Body { get; set; }
        public List<string> Eyes { get; set; }
        public List<string> Nose { get; set; }
        public List<string> Ears { get; set; }
        public List<string> Hair { get; set; }
        public List<string> Torso { get; set; }
        public List<string> Legs { get; set; }
        public List<string> Feet { get; set; }
        public List<string> Arms { get; set; }
        public List<string> Hands { get; set; }
        public List<string> Weapon { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="genreKey"></param>
        /// <param name="raceKey"></param>
        /// <param name="race"></param>
        public GenericSpriteBuilder(int key, int genreKey, int raceKey, string race)
        {
            _race = race;

            var seed = (new HashFunction()).MultiHash(genreKey, raceKey, key);
            _mersenneTwister = Randoms.Create(seed);

            FilterRaceDependantSpriteSheets();
        }

        /// <summary>
        /// 
        /// </summary>
        private void FilterRaceDependantSpriteSheets()
        {
            var myRegex = new Regex(@"^.*\\body\\" + _race + "\\\\(female|male).*$");
            Body = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();

            myRegex = new Regex(@"^.*\\body\\" + _race + "\\\\nose.*$");
            Nose = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();

            myRegex = new Regex(@"^.*\\body\\" + _race + "\\\\ears.*$");
            Ears = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();

            FilterSpriteSheets();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spriteSheets"></param>
        /// <param name="genre"></param>
        /// <returns></returns>
        private List<string> FilterGenreDependantSpriteSheets(List<string> spriteSheets, string genre)
        {
            var myRegex = new Regex(@"^.*\\" + genre + ".*$");
            return spriteSheets.Where(f => myRegex.IsMatch(f)).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        private void FilterSpriteSheets()
        {
            var myRegex = new Regex(@"^.*\\eyes.*$");
            Eyes = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();
            myRegex = new Regex(@"^.*\\hair.*$");
            Hair = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();
            myRegex = new Regex(@"^.*\\torso.*$");
            Torso = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();
            myRegex = new Regex(@"^.*\\legs.*$");
            Legs = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();
            myRegex = new Regex(@"^.*\\feet.*$");
            Feet = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();
            myRegex = new Regex(@"^.*\\arms.*$");
            Arms = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();
            myRegex = new Regex(@"^.*\\hands.*$");
            Hands = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();
            myRegex = new Regex(@"^.*\\weapon.*$");
            Weapon = _config.SpriteSheets.Where(f => myRegex.IsMatch(f)).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Sprite BuildFemaleSprite()
        {
            return BuildSprite(Genre.Female);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Sprite BuildMaleSprite()
        {
            return BuildSprite(Genre.Male);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="genre"></param>
        /// <returns></returns>
        private Sprite BuildSprite(string genre)
        {
            var sprite = new Sprite
            (
                ChooseSpriteSheet(FilterGenreDependantSpriteSheets(Body, genre)),
                ChooseSpriteSheet(Eyes),
                ChooseSpriteSheet(Nose),
                ChooseSpriteSheet(Ears),
                ChooseSpriteSheet(FilterGenreDependantSpriteSheets(Hair, genre)),
                ChooseSpriteSheet(FilterGenreDependantSpriteSheets(Torso, genre)),
                ChooseSpriteSheet(FilterGenreDependantSpriteSheets(Legs, genre)),
                ChooseSpriteSheet(FilterGenreDependantSpriteSheets(Feet, genre)),
                ChooseSpriteSheet(FilterGenreDependantSpriteSheets(Arms, genre)),
                ChooseSpriteSheet(FilterGenreDependantSpriteSheets(Hands, genre)),
                ChooseSpriteSheet(FilterGenreDependantSpriteSheets(Weapon, genre))
            );

            return sprite;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spriteSheetFiles"></param>
        /// <returns></returns>
        private string ChooseSpriteSheet(IReadOnlyList<string> spriteSheetFiles)
        {
            return spriteSheetFiles[_mersenneTwister.Next(spriteSheetFiles.Count)];
        }
    }
}
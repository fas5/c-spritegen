﻿using System.Collections.Generic;
using System.Linq;
using SpriteGen.Models.Constants;
using SpriteGen.Models.Sprites;

namespace SpriteGen.Models.Builder
{
    public class SpriteBuilder
    {
        private readonly Configuration _config = Configuration.GetInstance();

        private readonly List<GenericSpriteBuilder> _builders;

        private readonly List<string> _useGenre;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="useGenre"></param>
        /// <param name="useRace"></param>
        public SpriteBuilder(int key, IEnumerable<string> useGenre, IEnumerable<string> useRace)
        {
            _useGenre = useGenre.OrderBy(s => s).ToList();
            var useRaceOrdered = useRace.OrderBy(s => s).ToList();

            _builders = new List<GenericSpriteBuilder>();
            foreach (var race in useRaceOrdered)
            {
                AddBuilder(key, DefineGenreKey(_useGenre), race);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Sprite> BuildSprites()
        {
            var spriteList = new List<Sprite>();

            while (spriteList.Count() < _config.SpriteQuantity)
            {
                foreach (var genre in _useGenre)
                {
                    foreach (var builder in _builders)
                    {
                        if (spriteList.Count() < _config.SpriteQuantity)
                        {
                            spriteList.Add(ChooseGenre(builder, genre));
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            return spriteList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="genreKey"></param>
        /// <param name="race"></param>
        private void AddBuilder(int key, int genreKey, string race)
        {
            switch (race)
            {
                case Race.Dark:
                    _builders.Add(new GenericSpriteBuilder(key, genreKey, 1, Race.Dark));
                    break;
                case Race.Dark2:
                    _builders.Add(new GenericSpriteBuilder(key, genreKey, 2, Race.Dark2));
                    break;
                case Race.DarkElf:
                    _builders.Add(new GenericSpriteBuilder(key, genreKey, 3, Race.DarkElf));
                    break;
                case Race.DarkElf2:
                    _builders.Add(new GenericSpriteBuilder(key, genreKey, 4, Race.DarkElf2));
                    break;
                case Race.Light:
                    _builders.Add(new GenericSpriteBuilder(key, genreKey, 5, Race.Light));
                    break;
                case Race.Orc:
                    _builders.Add(new GenericSpriteBuilder(key, genreKey, 6, Race.Orc));
                    break;
                case Race.RedOrc:
                    _builders.Add(new GenericSpriteBuilder(key, genreKey, 7, Race.RedOrc));
                    break;
                case Race.Skeleton:
                    _builders.Add(new GenericSpriteBuilder(key, genreKey, 8, Race.Skeleton));
                    break;
                case Race.Tanned:
                    _builders.Add(new GenericSpriteBuilder(key, genreKey, 9, Race.Tanned));
                    break;
                case Race.Tanned2:
                    _builders.Add(new GenericSpriteBuilder(key, genreKey, 10, Race.Tanned2));
                    break;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="genre"></param>
        /// <returns></returns>
        private Sprite ChooseGenre(GenericSpriteBuilder builder, string genre)
        {
            Sprite sprite = null;

            switch (genre)
            {
                case Genre.Female:
                    sprite = builder.BuildFemaleSprite();
                    break;
                case Genre.Male:
                    sprite = builder.BuildMaleSprite();
                    break;
            }

            return sprite;
        }

        private int DefineGenreKey(ICollection<string> useGenre)
        {
            var genreKey = 1;

            if (useGenre.Count == 2)
            {
                genreKey = 3;
            }
            else if (useGenre.Contains(Genre.Female))
            {
                genreKey = 2;
            }

            return genreKey;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using SpriteGen.Models.Sprites;
using System.Web;
using SpriteGen.Models.Constants;

namespace SpriteGen.Models
{
    public class Util
    {
        private static readonly Configuration Config = Configuration.GetInstance();

        public static void CropSprites(List<Sprite> spriteList, int x, int y, int width, int height)
        {
            foreach (var sprite in spriteList)
            {
                sprite.CutBody = CutSpriteSheet(new Bitmap(sprite.OriginalBody), x, y, width, height);
                sprite.CutNose = CutSpriteSheet(new Bitmap(sprite.OriginalNose), x, y, width, height);
                sprite.CutEars = CutSpriteSheet(new Bitmap(sprite.OriginalEars), x, y, width, height);
                sprite.CutEyes = CutSpriteSheet(new Bitmap(sprite.OriginalEyes), x, y, width, height);
                sprite.CutHair = CutSpriteSheet(new Bitmap(sprite.OriginalHair), x, y, width, height);
                sprite.CutTorso = CutSpriteSheet(new Bitmap(sprite.OriginalTorso), x, y, width, height);
                sprite.CutLegs = CutSpriteSheet(new Bitmap(sprite.OriginalLegs), x, y, width, height);
                sprite.CutFeet = CutSpriteSheet(new Bitmap(sprite.OriginalFeet), x, y, width, height);
                sprite.CutArms = CutSpriteSheet(new Bitmap(sprite.OriginalArms), x, y, width, height);
                sprite.CutHands = CutSpriteSheet(new Bitmap(sprite.OriginalHands), x, y, width, height);
                sprite.CutWeapon = CutSpriteSheet(new Bitmap(sprite.OriginalWeapon), x, y, width, height);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sprite"></param>
        /// <param name="animationType"></param>
        /// <returns></returns>
        public static SpriteAnimation BuildAnimation(Sprite sprite, string animationType)
        {
            var spriteAnimation = new SpriteAnimation();

            switch (animationType)
            {
                case AnimationType.Walk:
                    for (var i = 0; i <= 8; i++)
                    {
                        spriteAnimation.BackFrameList.Add(CutSpriteSheet((Bitmap)sprite.MergedSpriteSheet, i * Config.SpriteWidth, 512, Config.SpriteWidth, Config.SpriteHeight));
                        spriteAnimation.LeftFrameList.Add(CutSpriteSheet((Bitmap)sprite.MergedSpriteSheet, i * Config.SpriteWidth, 576, Config.SpriteWidth, Config.SpriteHeight));
                        spriteAnimation.FrontFrameList.Add(CutSpriteSheet((Bitmap)sprite.MergedSpriteSheet, i * Config.SpriteWidth, 640, Config.SpriteWidth, Config.SpriteHeight));
                        spriteAnimation.RightFrameList.Add(CutSpriteSheet((Bitmap)sprite.MergedSpriteSheet, i * Config.SpriteWidth, 704, Config.SpriteWidth, Config.SpriteHeight));
                    }
                    break;
                case AnimationType.Slash:
                    break;
                case AnimationType.Shoot:
                    break;
                case AnimationType.Spellcast:
                    break;
                case AnimationType.Thrust:
                    break;
                case AnimationType.Hurt:
                    break;
            }

            return spriteAnimation;
        }

        ///<summary>
        /// Crops the portion of the sprite sheet relative to the rectangle which width
        /// and height are given as parameter, starting at point (x, y), and returns
        /// a byte array so it can be shown on a View.
        ///</summary>
        public static string CutSpriteSheet(Bitmap spriteSheet, int x, int y, int width, int height)
        {
            var original = spriteSheet;
            var rectangle = new Rectangle(x, y, width, height);
            var cropped = original.Clone(rectangle, original.PixelFormat);
            cropped = ResizeImage(cropped, 128, 128);

            var converter = new ImageConverter();

            var byteArray = (byte[])converter.ConvertTo(cropped, typeof(byte[]));

            return ByteArrayToBase64(byteArray);
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public static void SaveAsMultipleSpriteSheets(Sprite sprite)
        {
            SaveAllAsMultipleSpriteSheets(new List<Sprite> { sprite });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spriteList"></param>
        public static void SaveAllAsMultipleSpriteSheets(List<Sprite> spriteList)
        {
            var dateTimeNow = DateTime.Now.ToString("yyyyMMdd-HHmmss");

            var root = string.Concat(Config.TemporaryDir, "\\");
            var generatedSpritesDir = string.Concat("GeneratedSprites-", dateTimeNow);
            var outputDirectory = string.Concat(root, generatedSpritesDir, "\\");
            var zipFileName = string.Concat(generatedSpritesDir, ".zip");
            var zipFullPath = string.Concat(root, zipFileName);

            // Export sprite sheets
            ExportUnmergedSpriteSheets(spriteList, outputDirectory);

            // Zip sprite sheets
            CompactFiles(outputDirectory, zipFullPath);

            // Show download
            ShowDownloadWindow(zipFileName, zipFullPath);

            // Delete output folder and .zip file
            DeleteTempFiles(outputDirectory, zipFullPath);
        }

        public static void SaveAsSingleSpriteSheet(Sprite sprite)
        {
            SaveAllAsSingleSpriteSheet(new List<Sprite> { sprite });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spriteList"></param>
        public static void SaveAllAsSingleSpriteSheet(List<Sprite> spriteList)
        {
            var dateTimeNow = DateTime.Now.ToString("yyyyMMdd-HHmmss");

            var root = string.Concat(Config.TemporaryDir, "\\");
            var generatedSpritesDir = string.Concat("GeneratedSprites-", dateTimeNow);
            var outputDirectory = string.Concat(root, generatedSpritesDir, "\\");
            var zipFileName = string.Concat(generatedSpritesDir, ".zip");
            var zipFullPath = string.Concat(root, zipFileName);

            // Export sprite sheets
            ExportMergedSpriteSheets(spriteList, outputDirectory);

            // Zip sprite sheets
            CompactFiles(outputDirectory, zipFullPath);

            // Show download
            ShowDownloadWindow(zipFileName, zipFullPath);

            // Delete output folder and .zip file
            DeleteTempFiles(outputDirectory, zipFullPath);
        }

        private static void ExportUnmergedSpriteSheets(IEnumerable<Sprite> spriteList, string outputDirectory)
        {
            foreach (var sprite in spriteList)
            {
                var spriteDirectory = string.Concat(outputDirectory, sprite.Id, "\\");
                Directory.CreateDirectory(spriteDirectory);

                foreach (var spriteSheet in sprite.OriginalSpriteSheets)
                {
                    var image = new Bitmap(spriteSheet);
                    SaveImage(image, spriteDirectory, Path.GetFileName(spriteSheet));
                    image.Dispose();
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spriteList"></param>
        /// <param name="outputDirectory"></param>
        private static void ExportMergedSpriteSheets(IEnumerable<Sprite> spriteList, string outputDirectory)
        {
            foreach (var sprite in spriteList)
            {
                if (sprite.MergedSpriteSheet == null)
                {
                    sprite.MergedSpriteSheet = Util.MergeSpriteSheets(sprite);
                }

                SaveImage(sprite.MergedSpriteSheet, outputDirectory, string.Concat(sprite.Id, ".png"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="byteArray"></param>
        /// <returns></returns>
        public static string ByteArrayToBase64(byte[] byteArray)
        {
            var base64 = Convert.ToBase64String(byteArray);

            return String.Format("data:image/png;base64,{0}", base64);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static byte[] ImageToByteArray(Image image)
        {
            var memoryStream = new MemoryStream();
            image.Save(memoryStream, ImageFormat.Png);

            return memoryStream.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputDirectory"></param>
        /// <param name="zipFullPath"></param>
        private static void DeleteTempFiles(string outputDirectory, string zipFullPath)
        {
            // Delete output folder
            Directory.Delete(outputDirectory, true);
            // Delete .zip file
            File.Delete(zipFullPath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipFileName"></param>
        /// <param name="zipFullPath"></param>
        private static void ShowDownloadWindow(string zipFileName, string zipFullPath)
        {
            var byteArray = File.ReadAllBytes(zipFullPath);
            var response = HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.AddHeader("Content-Disposition", "inline; filename=" + zipFileName);
            response.ContentType = "application/zip";
            response.BinaryWrite(byteArray);
            response.End();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sprite"></param>
        public static Image MergeSpriteSheets(Sprite sprite)
        {
            var destRect = new Rectangle(0, 0, Config.SpriteSheetWidth, Config.SpriteSheetHeight);
            var destImage = new Bitmap(Config.SpriteSheetWidth, Config.SpriteSheetHeight);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceOver;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    Image image = new Bitmap(sprite.OriginalSpriteSheets[0]);
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);

                    foreach (var spriteSheet in sprite.OriginalSpriteSheets)
                    {
                        var nextImage = new Bitmap(spriteSheet);
                        graphics.DrawImage(nextImage, destRect, 0, 0, image.Width, image.Height,
                            GraphicsUnit.Pixel, wrapMode);

                        nextImage.Dispose();
                    }

                    image.Dispose();
                }
            }

            return destImage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        private static void SaveImage(Image image, string directory, string fileName)
        {
            Directory.CreateDirectory(directory);
            image.Save(string.Concat(directory, fileName), ImageFormat.Png);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        private static void CompactFiles(string directory, string fileName)
        {
            ZipFile.CreateFromDirectory(directory, fileName);

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using SpriteGen.Models.Constants;

namespace SpriteGen.Models
{
    /// <summary>
    /// Singleton responsible for maintaining the system's configurations.
    /// </summary>
    public class Configuration
    {
        private static Configuration Instance { get; set; }

        public string TemporaryDir = string.Concat(Path.GetTempPath(), Assembly.GetCallingAssembly().GetName().Name);
        public int SpriteSheetWidth { get; set; } = 832;
        public int SpriteSheetHeight { get; set; } = 1344;
        public int SpriteWidth { get; set; }
        public int SpriteHeight { get; set; }
        public int SpriteQuantity { get; set; }

        public Repository Repo { get; set; }

        public IEnumerable<string> SpriteSheets { get; set; }

        /// <summary>
        /// Private constructor.
        /// </summary>
        private Configuration()
        {
            SpriteWidth = 64;
            SpriteHeight = 64;
            Repo = new Repository();
            Directory.CreateDirectory(TemporaryDir);
            ReadAllFiles(SpriteSheetDirectories.Root);
        }

        /// <summary>
        /// Returns an instance of the Configuration class.
        /// </summary>
        /// <returns>An instance of the Configuration class.</returns>
        public static Configuration GetInstance()
        {
            return Instance ?? (Instance = new Configuration());
        }

        /// <summary>
        /// Saves the configuration.
        /// </summary>
        /// <param name="width">The sprite's width.</param>
        /// <param name="height">The sprite's height.</param>
        public void SaveConfig(string width, string height)
        {
            SpriteWidth = Convert.ToInt32(width);
            SpriteHeight = Convert.ToInt32(height);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        private void ReadAllFiles(string directory)
        {
            SpriteSheets = RecoverSpriteSheetFiles(directory, SpriteSheetDirectories.SpriteSheetFileType,
                SearchOption.AllDirectories);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileExtension"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        private IEnumerable<string> RecoverSpriteSheetFiles(string directory, string fileExtension, SearchOption option)
        {
            return Directory.GetFiles(HttpContext.Current.Server.MapPath(directory),
                fileExtension, option).ToList();
        }
    }
}
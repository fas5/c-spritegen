﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SpriteGen.Models.Sprites;

namespace SpriteGen.Models
{
    public class Repository
    {
        public List<Sprite> GeneratedSpriteList { get; set; }
    }
}
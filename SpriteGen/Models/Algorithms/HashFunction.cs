﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpriteGen.Models.Algorithms
{
    /// <summary>
    /// Integer Hash Function by Thomas Wang, Jan 1997
    /// @https://gist.github.com/badboy/6267743
    /// More @http://burtleburtle.net/bob/hash/integer.html
    /// </summary>
    public class HashFunction
    {
        /// <summary>
        /// Hashes a key in 6 shifts (provided you use the low bits, hash & (SIZE-1), 
        /// rather than the high bits if you can't use the whole value).
        /// </summary>
        /// <param name="key">Integer to be hashed.</param>
        /// <returns>Hash integer according to key.</returns>
        public int Hash(int key)
        {
            key += ~(key << 15);
            key ^= (key >> 10);
            key += (key << 3);
            key ^= (key >> 6);
            key += ~(key << 11);
            key ^= (key >> 16);

            return key;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public int MultiHash(int a, int b, int key)
        {
            return Hash(a ^ Hash(b ^ key));
        }
    }
}
﻿using System.Web;
using System.Web.Optimization;

namespace SpriteGen
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                "~/Scripts/knockout-3.4.2.js"));

            bundles.Add(new ScriptBundle("~/bundles/blockui").Include(
                "~/Scripts/jquery.blockUI.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/sweetalert").Include(
                "~/Scripts/sweetalert2.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/block").Include(
                "~/Content/scripts/block.js"));

            bundles.Add(new ScriptBundle("~/bundles/ajax").Include(
                "~/Content/scripts/ajax.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.css",
                      "~/Content/css/site.css",
                      "~/Content/css/font-awesome.min.css",
                      "~/Content/css/sweetalert2.min.css",
                      "~/Content/css/style.css"));
        }
    }
}

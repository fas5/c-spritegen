﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SpriteGen.Models;

namespace SpriteGen
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static Configuration Config { get; set; }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Config = Configuration.GetInstance();
        }
    }
}

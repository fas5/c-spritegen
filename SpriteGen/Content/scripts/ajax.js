﻿var ajaxJs = ajaxJs || {};

$(function (ajaxJs) {
    ajaxJs.ajax = function(type, url, parameters, callback, callbackError, async) {
        $.ajax({
            type: type,
            url: url,
            data: ko.toJSON(parameters),
            dataType: "json",
            async: async,
            contentType: "application/json; charset=utf-8",
            success: function(retorno) {
                callback(retorno);
            },
            error: function() {
                callbackError();
            }
        });
    }
}(ajaxJs));
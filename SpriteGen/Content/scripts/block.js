﻿var blockJs = blockJs || {};

blockJs.block = function (elementId) {
    var eId = elementId ? elementId : "pageBody";
    $("#" + eId).block({
        message: "<img src='/Content/Images/loading.gif' />",
        css: {
            border: "none",
            padding: "10px",
            backgroundColor: "none",
            '-webkit-border-radius': "5px",
            '-moz-border-radius': "5px",
            opacity: 1,
            color: "#fff",
            'z-index': "2001"
        },
        overlayCSS: {
            'z-index': "2000"
        }
    });
};

blockJs.unblock = function (elementId, opts) {
    var eId = elementId ? elementId : "pageBody";
    $("#" + eId).unblock(opts);
};
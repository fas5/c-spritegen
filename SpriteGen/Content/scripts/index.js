﻿// Represents the options a user can choose to generate the sprites.
var data = {
    "Genres": {
        "genreArray": ["1", "2"]
    },
    "Races": {
        "raceArray": ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    }
};

// List of <canvas> that show the sprite animations.
var canvasList = [
    document.getElementById("backAnimationCanvas"),
    document.getElementById("leftAnimationCanvas"),
    document.getElementById("frontAnimationCanvas"),
    document.getElementById("rightAnimationCanvas")
];

var showErrorAlert = function (message) {
    swal({
        type: "error",
        title: "Oh no!",
        text: message,
        confirmButtonText: "Ok!"
    });
};

var showWarningAlert = function (message) {
    swal({
        title: "Oops!",
        text: message,
        imageUrl: "/Content/Images/exclamation-point.png",
        imageWidth: 220,
        imageHeight: 160,
        confirmButtonText: "Ok!"
    });
}

Number.isPositiveInteger = function (value) {
    return typeof value === "string" &&
        isFinite(value) &&
        Math.floor(value).toString() === value &&
        parseInt(value) > 0;
};

Number.isInvalid = function (value) {
    return value == null || value === "" || isNaN(value) || !Number.isPositiveInteger(value);
}

// Sprite object.
var Sprite = function () {
    var self = this;
    // Identifier
    self.id = ko.observable();
    // Cut Sprites
    self.cutBody = ko.observable();
    self.cutNose = ko.observable();
    self.cutEars = ko.observable();
    self.cutEyes = ko.observable();
    self.cutHair = ko.observable();
    self.cutTorso = ko.observable();
    self.cutLegs = ko.observable();
    self.cutFeet = ko.observable();
    self.cutArms = ko.observable();
    self.cutHands = ko.observable();
    self.cutWeapon = ko.observable();
    // Original Spritesheets
    self.originalBody = ko.observable();
    self.originalNose = ko.observable();
    self.originalEars = ko.observable();
    self.originalEyes = ko.observable();
    self.originalHair = ko.observable();
    self.originalTorso = ko.observable();
    self.originalLegs = ko.observable();
    self.originalFeet = ko.observable();
    self.originalArms = ko.observable();
    self.originalHands = ko.observable();
    self.originalWeapon = ko.observable();
    // Merged Spritesheet (base 64)
    self.mergedSpriteSheetBase64 = ko.observable();
};

// Sprite animation object.
var jsSprite = function(options) {
    var self = {},
        frameIndex = 0,
        tickCount = 0,
        ticksPerFrame = 5,
        numberOfFrames = options.numberOfFrames || 1;

    self.context = options.context;
    self.xCoord = options.xCoord;
    self.width = options.width;
    self.height = options.height;
    self.image = options.image;
    self.loop = options.loop;

    // Renders a frame, part of the animation.
    self.render = function () {
        self.context.clearRect(0, 0, self.width, self.height);

        self.context.drawImage(
            self.image,
            frameIndex * self.width,
            self.xCoord,
            self.width,
            self.height,
            0,
            0,
            self.width,
            self.height);
    };

    // Updates the object so the next frame can be shown.
    self.update = function() {
        tickCount += 1;

        if (tickCount > ticksPerFrame) {
            tickCount = 0;

            if (frameIndex < numberOfFrames - 1) {
                frameIndex += 1;
            } else if (self.loop) {
                frameIndex = 0;
            }
        }
    };

    return self;
}

/*
 * Returns the spritesheet coordinates to be used on animation.
 * @parameter action The coordinates for each animation on the spritesheet.
 * @returns The spritesheet coordinates to be used on animation.
 */
var selectXCoord = function(action) {
    switch(action) {
        case "spellcast":
            return [0, 64, 128, 192];
        case "thrust":
            return [256, 320, 384, 448];
        case "walk":
            return [512, 576, 640, 704];
        case "slash":
            return [768, 832, 896, 960];
        case "shoot":
            return [1024, 1088, 1152, 1216];
        default:
            return [512, 576, 640, 704];
    }
};

var selectPerspective = function(canvasId, coordArray) {
    switch (canvasId) {
        case "backAnimationCanvas":
            return coordArray[0];
        case "leftAnimationCanvas":
            return coordArray[1];
        case "frontAnimationCanvas":
            return coordArray[2];
        case "rightAnimationCanvas":
            return coordArray[3];
        default:
            return coordArray[2];
    }
}

var animateSprite = function (canvasId, action, framesQuantity) {
    var canvas = document.getElementById(canvasId);
    canvas.width = 64;
    canvas.height = 64;

    var spriteSheet = new Image();
    spriteSheet.src = document.getElementById("modalSpriteSheet").src;

    var coord = action === "hurt" ? 1280 : selectPerspective(canvasId, selectXCoord(action));

    var test = jsSprite({
        context: canvas.getContext("2d"),
        xCoord: coord,
        width: 64,
        height: 64,
        image: spriteSheet,
        loop: true,
        numberOfFrames: framesQuantity
    });

    var loop = function() {
        window.requestAnimationFrame(loop);

        test.update();
        test.render();
    }

    spriteSheet.addEventListener("load", loop);
};

var animate = function (action, numberOfFrames) {
    animateSprite("backAnimationCanvas", action, numberOfFrames);
    animateSprite("leftAnimationCanvas", action, numberOfFrames);
    animateSprite("frontAnimationCanvas", action, numberOfFrames);
    animateSprite("rightAnimationCanvas", action, numberOfFrames);
}

function SpriteViewModel() {
    var key = ko.observable(),

        quantity = ko.observable(),

        useGenre = ko.observableArray(["female", "male"]),

        useRace = ko.observableArray(["dark", "dark2", "darkelf", "darkelf2", "light", "orc", "redorc", "skeleton", "tanned", "tanned2"]),

        showSaveAllAsMenu = ko.observable(false),

        spriteList = ko.observableArray([]),

        toBeSavedId = ko.observable(),

        mergedSpriteSheetBase64 = ko.observable();

    this.Genres = ko.observableArray([]);

    var generateSuccess = function (retorno) {
        retorno.forEach(function (item) {
            spriteList.push(new Sprite()
                // Identifier
                .id(item.Id)
                // Cut Sprites
                .cutBody(item.CutBody)
                .cutNose(item.CutNose)
                .cutEars(item.CutEars)
                .cutEyes(item.CutEyes)
                .cutHair(item.CutHair)
                .cutTorso(item.CutTorso)
                .cutLegs(item.CutLegs)
                .cutFeet(item.CutFeet)
                .cutArms(item.CutArms)
                .cutHands(item.CutHands)
                .cutWeapon(item.CutWeapon)
                // Original Spritesheets
                .originalBody(item.OriginalBody)
                .originalNose(item.OriginalNose)
                .originalEars(item.OriginalEars)
                .originalEyes(item.OriginalEyes)
                .originalHair(item.OriginalHair)
                .originalTorso(item.OriginalTorso)
                .originalLegs(item.OriginalLegs)
                .originalFeet(item.OriginalFeet)
                .originalArms(item.OriginalArms)
                .originalHands(item.OriginalHands)
                .originalWeapon(item.OriginalWeapon)
                // Merged Spritesheet (base 64)
                .mergedSpriteSheetBase64(item.MergedSpriteSheetBase64)
            );
        });

        showSaveAllAsMenu(true);

        blockJs.unblock();
    };

    var generateError = function () {
        showErrorAlert("An error has occured! Try again!");
        blockJs.unblock();
    }

    var generate = function () {
        blockJs.block();
        showSaveAllAsMenu(false);
        spriteList.removeAll();

        if (typeof useGenre() == "undefined" || useGenre().length === 0 ||
            typeof useRace() == "undefined" || useRace().length === 0) {

            showWarningAlert("You must select at least one Genre and Race!");

            blockJs.unblock();
            return;
        }

        if(Number.isInvalid(key())) {
            showWarningAlert("You must enter a positive integer on the Seed text box!");

            blockJs.unblock();
            return;
        }

        if (Number.isInvalid(quantity())) {
            showWarningAlert("You must enter a positive integer on the Quantity text box!");

            blockJs.unblock();
            return;
        }

        ajaxJs.ajax("POST", "Home/Generate", { key: key, quantity: quantity, useGenre: useGenre, useRace: useRace }, generateSuccess, generateError, true);
    };

    var saveAsSingleSpriteSheet = function () {
        window.location = "Home/SaveAsSingleSpriteSheet?id=" + toBeSavedId();
    };

    var saveAsMultipleSpriteSheets = function () {
        window.location = "Home/SaveAsMultipleSpriteSheets?id=" + toBeSavedId();
    };

    var saveAllAsSingleSpriteSheet = function () {
        window.location = "Home/SaveAllAsSingleSpriteSheet";
    };

    var saveAllAsMultipleSpriteSheets = function () {
        window.location = "Home/SaveAllAsMultipleSpriteSheets";
    };

    var displayCanvas = function (showBackCanvas, showLeftCanvas, showFrontCanvas, showRightCanvas) {
        showBackCanvas ? canvasList[0].style.display = "inline-block" : canvasList[0].style.display = "none";
        showLeftCanvas ? canvasList[1].style.display = "inline-block" : canvasList[1].style.display = "none";
        showFrontCanvas ? canvasList[2].style.display = "inline-block" : canvasList[2].style.display = "none";
        showRightCanvas ? canvasList[3].style.display = "inline-block" : canvasList[3].style.display = "none";
    }

    var showWalkAnimation = function () {
        displayCanvas(true, true, true, true);
        animate("walk", 9);
    }

    var showSlashAnimation = function () {
        displayCanvas(true, true, true, true);
        animate("slash", 6);
    }

    var showShootAnimation = function () {
        displayCanvas(true, true, true, true);
        animate("shoot", 13);
    }

    var showSpellcastAnimation = function () {
        displayCanvas(true, true, true, true);
        animate("spellcast", 7);
    }

    var showThrustAnimation = function () {
        displayCanvas(true, true, true, true);
        animate("thrust", 8);
    }

    var showHurtAnimation = function () {
        displayCanvas(false, false, true, false);
        animateSprite("frontAnimationCanvas", "hurt", 6);
    }

    var mergeSpriteSheetsSuccess = function(retorno) {
        mergedSpriteSheetBase64(retorno.MergedSpriteSheetBase64);
    }

    var ajaxMerge = function () {
        ajaxJs.ajax("POST", "Home/MergeSpriteSheets", { id: toBeSavedId() }, mergeSpriteSheetsSuccess, generateError, false);
    }

    var openModal = function (sprite) {
        canvasList.forEach(function(item) {
            item.width = 64;
            item.height = 64;

            var ctx = item.getContext("2d");
            item.width = item.width;
            item.height = item.height;
            ctx.clearRect(0, 0, item.width, item.height);
        });

        toBeSavedId(sprite.id());

        $.when(ajaxMerge()).done(function () {
            sprite.mergedSpriteSheetBase64(mergedSpriteSheetBase64());
            $("#modalSpriteSheet").attr("src", sprite.mergedSpriteSheetBase64());

            showWalkAnimation();
        });
    };

    return {
        key: key,
        quantity: quantity,
        useGenre: useGenre,
        useRace: useRace,
        showSaveAllAsMenu: showSaveAllAsMenu,
        spriteList: spriteList,
        toBeSavedId: toBeSavedId,
        mergedSpriteSheetBase64,
        generate: generate,
        saveAsSingleSpriteSheet: saveAsSingleSpriteSheet,
        saveAsMultipleSpriteSheets: saveAsMultipleSpriteSheets,
        saveAllAsSingleSpriteSheet: saveAllAsSingleSpriteSheet,
        saveAllAsMultipleSpriteSheets: saveAllAsMultipleSpriteSheets,
        openModal: openModal,
        showWalkAnimation: showWalkAnimation,
        showSlashAnimation: showSlashAnimation,
        showShootAnimation: showShootAnimation,
        showSpellcastAnimation: showSpellcastAnimation,
        showThrustAnimation: showThrustAnimation,
        showHurtAnimation: showHurtAnimation
    };
}

ko.applyBindings(new SpriteViewModel(), $("#content")[0]);
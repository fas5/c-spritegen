﻿function cancel() {
    location.href = ("/Configuration/Cancel");
}

function save() {
    var spriteWidth = $("#inputSpriteWidth").val();
    var spriteHeight = $("#inputSpriteHeight").val();

    location.href = ("/Configuration/Save?width=" + spriteWidth + "&height=" + spriteHeight);
}
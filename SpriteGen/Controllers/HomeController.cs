﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SpriteGen.Models;
using SpriteGen.Models.Builder;

namespace SpriteGen.Controllers
{
    public class HomeController : Controller
    {
        private readonly Configuration _config = Configuration.GetInstance();

        public ActionResult Generate(int key, int quantity, List<string> useGenre, List<string> useRace)
        {
            _config.SpriteQuantity = quantity;

            var spriteBuilder = new SpriteBuilder(key, useGenre, useRace);

            var spriteList = spriteBuilder.BuildSprites();

            _config.Repo.GeneratedSpriteList = spriteList;

            Util.CropSprites(spriteList, 0, 640, _config.SpriteWidth, _config.SpriteHeight);

            return Json(spriteList);
        }

        public void SaveAsSingleSpriteSheet(string id)
        {
            var sprite = _config.Repo.GeneratedSpriteList.First(item => item.Id == id);

            Util.SaveAsSingleSpriteSheet(sprite);
        }

        public void SaveAsMultipleSpriteSheets(string id)
        {
            var sprite = _config.Repo.GeneratedSpriteList.First(item => item.Id == id);

            Util.SaveAsMultipleSpriteSheets(sprite);
        }

        public void SaveAllAsSingleSpriteSheet()
        {
            Util.SaveAllAsSingleSpriteSheet(_config.Repo.GeneratedSpriteList);
        }

        public void SaveAllAsMultipleSpriteSheets()
        {
            Util.SaveAllAsMultipleSpriteSheets(_config.Repo.GeneratedSpriteList);
        }

        public ActionResult MergeSpriteSheets(string id)
        {
            var sprite = _config.Repo.GeneratedSpriteList.First(item => item.Id == id);

            sprite.MergedSpriteSheet = Util.MergeSpriteSheets(sprite);
            var byteArray = Util.ImageToByteArray(sprite.MergedSpriteSheet);
            sprite.MergedSpriteSheetBase64 = Util.ByteArrayToBase64(byteArray);

            return Json(sprite);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SpriteGen.Controllers
{
    public class ConfigurationController : Controller
    {
        // GET: Configuration
        public ActionResult Configuration()
        {
            ViewBag.Message = "Your configuration page.";

            return View();
        }

        public ActionResult Save(string width, string height)
        {
            MvcApplication.Config.SaveConfig(width, height);

            return Cancel();
        }

        public ActionResult Cancel()
        {
            return View("~/Views/Home/Index.cshtml");
        }
    }
}